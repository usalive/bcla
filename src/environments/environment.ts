// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //websiteURL: (<HTMLInputElement>document.getElementById('websiteURL')).value
  
  TPSSOCIETYID: (<HTMLInputElement>document.getElementById('TPSSOCIETYID')).value,
  TPSHOSTNAME: (<HTMLInputElement>document.getElementById('TPSHOSTNAME')).value,
  TPSMEMBERLEVEL: (<HTMLInputElement>document.getElementById('TPSMEMBERLEVEL')).value,
  TARGET_JOURNAL_URL: (<HTMLInputElement>document.getElementById('TARGET_JOURNAL_URL')).value,
  TPSENCRYPTKEY : (<HTMLInputElement>document.getElementById('TPSENCRYPTKEY')).value,
  TOKEN_PARAM_NAME : (<HTMLInputElement>document.getElementById('TOKEN_PARAM_NAME')).value
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
