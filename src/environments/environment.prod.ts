export const environment = {
  production: true,
  TPSSOCIETYID: (<HTMLInputElement>document.getElementById('TPSSOCIETYID')).value,
  TPSHOSTNAME: (<HTMLInputElement>document.getElementById('TPSHOSTNAME')).value,
  TPSMEMBERLEVEL: (<HTMLInputElement>document.getElementById('TPSMEMBERLEVEL')).value,
  TARGET_JOURNAL_URL: (<HTMLInputElement>document.getElementById('TARGET_JOURNAL_URL')).value,
  TPSENCRYPTKEY : (<HTMLInputElement>document.getElementById('TPSENCRYPTKEY')).value,
  TOKEN_PARAM_NAME : (<HTMLInputElement>document.getElementById('TOKEN_PARAM_NAME')).value


};
