import { Component, OnInit } from '@angular/core';
var _forge = require('node-forge');
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  title = 'Angular-EncryptDecrypt';
  //public gIV_128: number[];
  //public gInput: string;
  //public gencrypted: string;
  //public array: Uint8Array;
  //public encryptionKeyBytes: Uint8Array;
  //public gKey: Uint8Array;
  //public encryptDecrypt: string;
  //public EncryptDecryptText: string;

  constructor(private router: Router) {
    //this.gIV_128 = [55, 103, 246, 79, 136, 99, 167, 3, 42, 205, 62, 183, 184, 7, 109, 13];
    //this.gInput = "";
    //this.gencrypted = "";
    //this.encryptDecrypt = "";
    //this.EncryptDecryptText = "";
  }

  ngOnInit() {
    var width = $("body").width();
    console.log('Constructor initialised');
  }


  //private EncryptText() {
  //  let textToEncrypt = this.encryptDecrypt;
  //  if (textToEncrypt !== "" && textToEncrypt !== null && textToEncrypt !== undefined) {
  //    this.encryption(textToEncrypt);
  //  }
  //}

  //private DecryptText() {
  //  let textToDecrypt = this.encryptDecrypt;
  //  if (textToDecrypt !== "" && textToDecrypt !== null && textToDecrypt !== undefined) {
  //    this.decryption(textToDecrypt);
  //  }
  //}

  //private convertDataURIToBinary(dataURI) {
  //  var base64 = dataURI;
  //  var raw = window.atob(base64);
  //  var rawLength = raw.length;
  //  var num: number = 0
  //  var count: number = 0;
  //  this.array = new Uint8Array(new ArrayBuffer(rawLength));
  //  for (num = 0; num < rawLength; num++) {
  //    this.array[num] = raw.charCodeAt(num);
  //  }
  //  return this.array;
  //}

  //private encryption(textToEncrypt: string) {
  //  //var forge = forge;
  //  var TPSSOCIETYID = "13";
  //  var TPSENCRYPTKEY = "b5700cb26d1feb66b5700cb26d1feb66";
  //  var TPSMEMBERLEVEL = "memberaccess";
  //  //var TPSHOSTNAME = "www.bcla.org.uk";
  //  var TPSHOSTNAME = textToEncrypt;
  //  var TARGET_JOURNAL_URL = "www.contactlensjournal.com";
  //  var encinput = TPSENCRYPTKEY;

  //  var now = new Date();
  //  var datefromAPITimeStamp = (new Date(Date.UTC(1970, 1, 1, 0, 0, 0)).getTime());

  //  var currentDate = new Date();
  //  var date = currentDate.getUTCDate();
  //  var month = currentDate.getUTCMonth() + 1; //Be careful! January is 0 not 1
  //  var year = currentDate.getUTCFullYear();
  //  var hours = currentDate.getUTCHours();
  //  var minutes = currentDate.getUTCMinutes();
  //  var seconds = currentDate.getUTCSeconds();
  //  var nowTimeStamp = (new Date(Date.UTC(year, month, date, hours, minutes, seconds)).getTime());

  //  var microSecondsDiff = Math.abs(nowTimeStamp - datefromAPITimeStamp);

  //  // Number of milliseconds per day =
  //  //   24 hrs/day * 60 minutes/hour * 60 seconds/minute * 1000 msecs/second
  //  var daysDiff = Math.floor(microSecondsDiff / (1000 * 60 * 60 * 24));

  //  //var input = TPSHOSTNAME + "|" + microSecondsDiff.toString() + "|" + TPSMEMBERLEVEL;
  //  var input = TPSHOSTNAME;
  //  this.gInput = input;

  //  this.encryptionKeyBytes = this.convertDataURIToBinary(encinput);
  //  //var IV_128 = [];
  //  //IV_128 = [55, 103, 246, 79, 136, 99, 167, 3, 42, 205, 62, 183, 184, 7, 109, 13];

  //  //key = encryptionKeyBytes;
  //  //iv = IV_128;
  //  this.gKey = this.encryptionKeyBytes;
  //  //this.gIV_128 = IV_128;

  //  var cipher = _forge.cipher.createCipher('3DES-ECB', this.gKey);
  //  cipher.start({ iv: this.gIV_128 });
  //  //cipher.update(forge.util.createBuffer(input, 'binary'));
  //  cipher.update(_forge.util.createBuffer(input));
  //  cipher.finish();

  //  var encrypted = cipher.output;

  //  //var data = _forge.util.bytesToHex(encrypted);
  //  //var obj = { 'iv': _forge.util.bytesToHex(this.gIV_128.toString()), 'encrypted': data };
  //  //console.log(JSON.stringify(obj));
  //  //this.gencrypted = JSON.stringify(obj);

  //  var HexTPS = (encrypted.toHex());
  //  var encodeTPS = (_forge.util.encode64(encrypted.getBytes()));
  //  var encodeURI = encodeURIComponent(encodeTPS);
  //  this.EncryptDecryptText = encodeURI;
  //  //console.log(HexTPS);
  //  console.log(encodeTPS);

  //  var TOKEN_PARAM_NAME = "tpstoken";
  //  var dynamicTPS = "http://" + TARGET_JOURNAL_URL + "/?" + TOKEN_PARAM_NAME + "=" + TPSSOCIETYID + "." + encodeURI
  //  var staticHTML = "<p><a class='btn' href=http://' " + dynamicTPS + " ' target='_blank'><span>CLAE Access</span></a></p> </div>";
  //  var aTag = dynamicTPS;

  //  this.router.navigate(['app-dec', this.EncryptDecryptText])

  //  //aTag = "http://www.contactlensjournal.com?tpstoken=13.GNQ50ZRBw%2fTC1BirSI7Dn5r6dEca9%2fBGvLVLTIOAmk%2bSTdfCqXY3PpMpL%2bgx1HJH";

  //}

  //private decryption(textToDecrypt: string) {
  //  //var DecodedtextToDecrypt = decodeURIComponent(textToDecrypt);
  //  var DecodedtextToDecrypt = _forge.util.decode64(decodeURIComponent(textToDecrypt));
  //  var dataToDecrypt1 = _forge.util.bytesToHex(DecodedtextToDecrypt);
  //  var dataToDecrypt2 = _forge.util.hexToBytes(dataToDecrypt1);

  //  var iv = _forge.util.createBuffer();
  //  var data = _forge.util.createBuffer();
  //  iv.putBytes(this.gIV_128);
  //  data.putBytes(dataToDecrypt2);
  //  this.gIV_128 = iv;

  //  var decipher = _forge.cipher.createDecipher('3DES-ECB', this.gKey);
  //  decipher.start({ iv: this.gIV_128 });
  //  decipher.update(data);
  //  var result = decipher.finish(); // check 'result' for true/false

  //  var encrypted = decipher.output;
  //  var decryptText = encrypted.data;
  //  this.EncryptDecryptText = decryptText;
  //  var HexTPS = (encrypted.toHex());
  //  var encodeTPS = (_forge.util.encode64(encrypted.getBytes()));
  //  var encodeURI = encodeURIComponent(encodeTPS);
  //  console.log(HexTPS);
  //  console.log(encodeTPS);
  //}

}

