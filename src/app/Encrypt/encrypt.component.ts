import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';
var _forge = require('node-forge');
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-enc',
  templateUrl: './encrypt.component.html',
})

export class EncryptComponent implements OnInit {
  public gIV_128: number[];
  public array: Uint8Array;
  public gKey: Uint8Array;
  public encryptionKeyBytes: Uint8Array;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.gIV_128 = [55, 103, 246, 79, 136, 99, 167, 3, 42, 205, 62, 183, 184, 7, 109, 13];
  }

  ngOnInit() {
    this.encryption();
  }

  private convertDataURIToBinary(dataURI) {
    var raw = window.atob(dataURI);
    var rawLength = raw.length;
    var num: number = 0
    var count: number = 0;
    this.array = new Uint8Array(new ArrayBuffer(rawLength));
    for (num = 0; num < rawLength; num++) {
      this.array[num] = raw.charCodeAt(num);
    }
    return this.array;
  }

  private encryption() {
    // var TPSSOCIETYID = "13";
    // var TPSENCRYPTKEY = "b5700cb26d1feb66b5700cb26d1feb66";
    // var TPSMEMBERLEVEL = "memberaccess";
    // var TPSHOSTNAME = "www.bcla.org.uk";
    // var TARGET_JOURNAL_URL = "www.contactlensjournal.com";
    var TPSSOCIETYID = environment.TPSSOCIETYID;
    var TPSENCRYPTKEY = environment.TPSENCRYPTKEY;
    var TPSMEMBERLEVEL = environment.TPSMEMBERLEVEL;
    var TPSHOSTNAME = environment.TPSHOSTNAME;
    var TOKEN_PARAM_NAME = environment.TOKEN_PARAM_NAME;
    var TARGET_JOURNAL_URL = environment.TARGET_JOURNAL_URL;
    var milliSecondsDiff = new Date().getTime();
    var daysDiff = Math.floor(milliSecondsDiff / (1000 * 60 * 60 * 24));
    var input = TPSHOSTNAME + "|" + milliSecondsDiff.toString() + "|" + TPSMEMBERLEVEL;
    this.encryptionKeyBytes = this.convertDataURIToBinary(TPSENCRYPTKEY);
    var cipher = _forge.cipher.createCipher('3DES-ECB',this.encryptionKeyBytes);
    cipher.start({ iv: this.gIV_128 });
    cipher.update(_forge.util.createBuffer(input));
    cipher.finish();
    var encrypted = cipher.output;
    var encodeTPS = (_forge.util.encode64(encrypted.getBytes()));
    var encodeURI = encodeURIComponent(encodeTPS);
    //var dynamicTPS=environment.websiteURL + "=" + TPSSOCIETYID + "." + encodeURI;
    var dynamicTPS =  TARGET_JOURNAL_URL + TOKEN_PARAM_NAME + "=" + TPSSOCIETYID + "." + encodeURI
    var staticHTML = "<p><a class='btn' href=http://' " + dynamicTPS + " ' target='_blank'><span>CLAE Access</span></a></p> </div>";
    var aTag = dynamicTPS;
    var encryptCodeDiv = $("#encryptCodeTag");
    $(encryptCodeDiv).attr('href', aTag);
  }
}
