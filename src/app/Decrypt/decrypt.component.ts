import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
var _forge = require('node-forge');
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-dec',
  templateUrl: './decrypt.component.html',
})


export class DecryptComponent implements OnInit, OnDestroy  {

  TextToDecrypt: string;
  DisplayDecryptText: string;
  private sub: any;
  public gIV_128: number[];
  public array: Uint8Array;
  public gKey: Uint8Array;
  public encryptionKeyBytes: Uint8Array;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.gIV_128 = [55, 103, 246, 79, 136, 99, 167, 3, 42, 205, 62, 183, 184, 7, 109, 13];
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.TextToDecrypt = params['id']; // (+) converts string 'id' to a number
      let textToDecrypt = this.TextToDecrypt;
      if (textToDecrypt !== "" && textToDecrypt !== null && textToDecrypt !== undefined) {
        this.decryption(textToDecrypt);
      }
    });
  }

  private convertDataURIToBinary(dataURI) {
    var base64 = dataURI;
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var num: number = 0
    var count: number = 0;
    this.array = new Uint8Array(new ArrayBuffer(rawLength));
    for (num = 0; num < rawLength; num++) {
      this.array[num] = raw.charCodeAt(num);
    }
    return this.array;
  }


  private decryption(textToDecrypt: string) {
    var TPSENCRYPTKEY = "b5700cb26d1feb66b5700cb26d1feb66";
    var encinput = TPSENCRYPTKEY;
    this.encryptionKeyBytes = this.convertDataURIToBinary(encinput);
    this.gKey = this.encryptionKeyBytes;
    var DecodedtextToDecrypt = _forge.util.decode64(decodeURIComponent(textToDecrypt));
    var dataToDecrypt1 = _forge.util.bytesToHex(DecodedtextToDecrypt);
    var dataToDecrypt2 = _forge.util.hexToBytes(dataToDecrypt1);
    var iv = _forge.util.createBuffer();
    var data = _forge.util.createBuffer();
    iv.putBytes(this.gIV_128);
    data.putBytes(dataToDecrypt2);
    this.gIV_128 = iv;
    var decipher = _forge.cipher.createDecipher('3DES-ECB', this.gKey);
    decipher.start({ iv: this.gIV_128 });
    decipher.update(data);
    var result = decipher.finish(); 
    var encrypted = decipher.output;
    var decryptText = encrypted.data;
    this.DisplayDecryptText = decryptText;
    var HexTPS = (encrypted.toHex());
    var encodeTPS = (_forge.util.encode64(encrypted.getBytes()));
    var encodeURI = encodeURIComponent(encodeTPS);
    console.log(HexTPS);
    console.log(encodeTPS);
  }

  public redirectToEncrypt() {
    this.router.navigate(['/app-enc'])
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
