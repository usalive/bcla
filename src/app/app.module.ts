import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { EncryptComponent } from './Encrypt/encrypt.component';
import { DecryptComponent } from './Decrypt/decrypt.component';
import { FormsModule } from '@angular/forms';
import * as $ from 'jquery';

const appRoutes: Routes = [

  //{
  //  path: '',
  //  //redirectTo: 'app-enc',
  //  component: EncryptComponent,
  //  //redirectTo: './encrypt.component.html',
  //  //pathMatch: 'full',
  //},

  ////for iMIS Default Path
  //// for Menu =>  Staff/Advertising/AEncryptDecrypt/AngularEncryptDecrypt.aspx
  //// for Content Page => iMIS/ContentManagement/ContentPreview.aspx

  // {
  //   //path: 'Staff/Advertising/AEncryptDecrypt/AngularEncryptDecrypt.aspx',
  //   path: 'Staff/Advertising/AngularSixEncryptDecrypt/AngularSixEncryptDecrypt.aspx',
  //   component: EncryptComponent,
  // },

  // {
  //   path: 'iMIS/ContentManagement/ContentPreview.aspx',
  //   component: EncryptComponent,
  // },

  ///// if not any url found then set noPageFound
  //{
  //  path: '.+',
  //  component: EncryptComponent,
  //},

  ///// if not any url found then set nocomponent found error page
  //  {
  //    path: '**',
  //    component: EncryptComponent,
  //  },

  // {
  //   path: 'app-enc',
  //   component: EncryptComponent
  // },
  // {
  //   path: 'app-dec/:id',
  //   component: DecryptComponent
  // }

];

@NgModule({
  declarations: [
    AppComponent, DecryptComponent, EncryptComponent
  ],
  imports: [
    BrowserModule, FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
